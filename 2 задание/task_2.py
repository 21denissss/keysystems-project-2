import requests
import json
import csv

# Название столбцов
outData = [["text", "number", "found", "type"]]

# Создание 100 выходных строк
for i in range(100):
    print(i.__str__())
    # Получение данных с сайта
    r = requests.get("http://numbersapi.com/" + i.__str__() + "/math?json").text

    # Парсинг json-данных
    jsonData = json.loads(r)

    # Добавление данных в конец списка
    outData.append([jsonData["text"], jsonData["number"], jsonData["found"], jsonData["type"]])

# Формирование списка на вывод
outList = []
fieldsname = outData[0]
cell = outData[1:]
for values in cell:
    inner_dict = dict(zip(fieldsname, values))
    outList.append(inner_dict)

# Создание выходного файла
path = "outfile.csv"
with open(path, "w", newline='', encoding='utf-8') as out_file:
    writer = csv.DictWriter(out_file, delimiter=';', fieldnames=fieldsname)
    writer.writeheader()
    for row in outList:
        writer.writerow(row)
