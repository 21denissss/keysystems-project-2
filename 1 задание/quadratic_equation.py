import math


class quadratic_equation:
    def __init__(self, a, b, c):
        self.a, self.b, self.c = a, b, c

    def discriminant(self):
        return self.b * self.b - 4 * self.a * self.c

    def calculate(self):
        d = self.discriminant()
        if d > 0:
            a = (-self.b + math.sqrt(d)) / (2 * self.a)
            b = (-self.b - math.sqrt(d)) / (2 * self.a)
            return a, b
        elif d == 0:
            a = (-self.b) / (2 * self.a)
            return a
        else:
            raise Exception("Отрицательный дискриминант")
