import unittest
from quadratic_equation import quadratic_equation


class test_quadratic_equation(unittest.TestCase):
    def test_discriminant_equally_zero(self):
        temp_object = quadratic_equation(1, -4, 4)
        self.assertEqual(temp_object.discriminant(), 0)

    def test_discriminant_more_zero(self):
        temp_object = quadratic_equation(3, 3, -6)
        self.assertEqual(temp_object.discriminant(), 81)

    def test_discriminant_less_zero(self):
        temp_object = quadratic_equation(3, 1, 6)
        self.assertEqual(temp_object.discriminant(), -71)

    def test_calculate_where_discriminant_equally_zero(self):
        temp_object = quadratic_equation(1, -4, 4)
        self.assertEqual(temp_object.calculate(), (2))

    def test_calculate_where_discriminant_more_zero(self):
        temp_object = quadratic_equation(3, 3, -6)
        self.assertEqual(temp_object.calculate(), (1, -2))

    def test_calculate_where_discriminant_less_zero(self):
        temp_object = quadratic_equation(3, 1, 6)
        with self.assertRaises(Exception):
            temp_object.calculate()
